const conexion = require('./connection')

module.exports = app => {
    const connection = conexion

    app.post('/register', (req, res) => {

        const {
            cedula,
            apellidos,
            nombres,
            direccion,
            telefono
        } = req.body
        connection.query('INSERT INTO usuario SET ?', {
            cedula, apellidos, nombres, direccion, telefono
        }, (err, result) => {
            res.send('Registro ingresado correctamente')
        })
    })
}
